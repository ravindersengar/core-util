package com.xlrs.commons.constant;

public class CommonConstants {
	
	
	public static final String INVALID_DATE 	= "Invalid Date";
	public static String BOOLEAN_TRUE 			= "true";
	public static String BOOLEAN_FALSE 			= "false";
	public static String STATUS_SUCCESS 		= "Success";
	public static String STATUS_FAILURE 		= "Failure";
	public static String SYSTEM_ERROR 			= "SYSTEM_ERROR";
	public static String REQUEST_KEY 			= "";

	public static String JSON_DILIMITER 		= ":";
	
	public static String STATUS_ACTIVE 			= "Active";
	public static String STATUS_INACTIVE 		= "Inactive";
	public static String SYSTEM_DELETED 		= "Deleted";
	
	public static String CONTENTTYPE_HTML 					= "HTML";
	public static String CONTENTTYPE_TEXT 					= "TEXT";
	public static String CONTENTTYPE_ATTACHMENT 			= "ATTACHMENT";
	public static String USER_WELCOME									= 	"WELCOME";
	public static String FORGET_PASS 										= "FORGET PASSWORD";
	
	public static final Long CREATED_BY 		= 1L;
	public static final String API_KEY 			= "X-API-KEY";
	
	
}
