package com.xlrs.commons.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.xlrs.commons.exception.DataFormatException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.exception.RequestValidationException;
import com.xlrs.commons.exception.ResourceNotFoundException;
import com.xlrs.commons.view.ErrorView;
import com.xlrs.commons.view.HTTPStatus;

import feign.FeignException;
import io.jsonwebtoken.JwtException;

/**
 * This class is meant to be extended by all REST resource "controllers". It
 * contains exception mapping and other common REST API functionality
 */
@ControllerAdvice
public abstract class AbstractRestHandler implements ApplicationEventPublisherAware {

	protected ApplicationEventPublisher eventPublisher;
	
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public @ResponseBody ErrorView handleException(Exception ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public @ResponseBody ErrorView handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.BAD_REQUEST, ex.getMessage());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(DataFormatException.class)
	public @ResponseBody ErrorView handleDataFormatException(DataFormatException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(ResourceNotFoundException.class)
	public @ResponseBody ErrorView handleResourceNotFoundException(ResourceNotFoundException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.NOT_FOUND, ex.getMessage());
	}
	
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler(NoResultFoundException.class)
	public @ResponseBody ErrorView handleNoResultFoundException(NoResultFoundException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.PRECONDITION_FAILED, ex.getMessage());
	}

	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler(RequestValidationException.class)
	public @ResponseBody ErrorView handleRequestValidationException(RequestValidationException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.PRECONDITION_FAILED, ex.getMessage());
	}
	
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(JwtException.class)
	public @ResponseBody ErrorView handleJwtException(JwtException ex) {
		ex.printStackTrace();
		return new ErrorView(HTTPStatus.UNAUTHORIZED, ex.getMessage());
	}
	
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler(FeignException.class)
    public @ResponseBody ErrorView handleFeignException(FeignException ex, HttpServletResponse response) {
        response.setStatus(ex.status());
        return new ErrorView(HTTPStatus.PRECONDITION_FAILED, ex.getMessage());
    }
	
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.eventPublisher = applicationEventPublisher;
	}

}