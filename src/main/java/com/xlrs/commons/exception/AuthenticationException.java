package com.xlrs.commons.exception;


public class AuthenticationException extends Exception {
	
	private static final long serialVersionUID = -8006630353359097191L;
	protected String message;

	public AuthenticationException(String message, Exception e) {
		e.printStackTrace();
		this.message = message;
	}
	
	public AuthenticationException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}
