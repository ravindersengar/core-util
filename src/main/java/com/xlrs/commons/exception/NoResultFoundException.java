package com.xlrs.commons.exception;

import org.springframework.web.bind.MethodArgumentNotValidException;

public class NoResultFoundException extends Exception {
	
	private static final long serialVersionUID = -8006630353359097191L;
	protected String message;
	
	public NoResultFoundException(String defualtSystemException, String validationException, Exception e) {
		
		if(e instanceof ValidationException ||e instanceof  MethodArgumentNotValidException) {
			this.message = validationException;
		} else  {
			e.printStackTrace();
			this.message = defualtSystemException;
		}
		
	}
	
	public NoResultFoundException(String defualtSystemException, Exception e) {
		e.printStackTrace();	
		this.message = defualtSystemException;
	}
	
	public NoResultFoundException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}
