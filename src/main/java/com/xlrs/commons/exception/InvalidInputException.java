package com.xlrs.commons.exception;

/**
 * for HTTP 412 errors
 */
public final class InvalidInputException extends RuntimeException implements ValidationException{
    
	private static final long serialVersionUID = 1L;
	protected String message;
	
	
	public InvalidInputException() {
        super();
    }

   
    public InvalidInputException(Throwable cause) {
        super(cause);
    }

	public InvalidInputException(String errorMessages) {
		this.message=errorMessages;
	}



	public String getMessage() {
		return this.message;
	}

}