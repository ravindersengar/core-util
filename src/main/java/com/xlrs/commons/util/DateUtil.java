package com.xlrs.commons.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.exception.ApplicationException;

public abstract class DateUtil {
	public static final String DATE_PATTERN = "dd-mm-yyyy";

	public static Date getDateFromStr(String date)
			throws ApplicationException {
		if(date==null){
			return null;
		}
		Date result = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
			result = sdf.parse(date);
		} catch (Exception e) {
			throw new ApplicationException(CommonConstants.INVALID_DATE, e);
		}
		return result;
	}
	
	public static java.sql.Date getSqlDateFromUtil(Date utilDate){
		if(utilDate==null){
			return null;
		}else{
			return new java.sql.Date(utilDate.getTime());
		}
	}
	
	public static java.sql.Date getSqlDateFromString(String date) throws ApplicationException{
		if(date==null){
			return null;
		}else{
			return new java.sql.Date(getDateFromStr(date).getTime());
		}
	}

	/***
	 * Convert Date to String in the given format
	 */
	public static String getStrFromDate(Date date)
			throws ApplicationException {
		if(date==null){
			return null;
		}
		String result = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
			result = dateFormat.format(date);
		} catch (Exception e) {
			throw new ApplicationException(CommonConstants.INVALID_DATE, e);
		}
		return result;
	}

	/***
	 * Return date in the given format
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentDate(String format) {
		// DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		return dateFormat.format(date);
	}

	/***
	 * Return date in the given format
	 * 
	 * @param format
	 * @return
	 */
	public static Timestamp getCurrentTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		Timestamp ourJavaTimestampObject = new Timestamp(calendar.getTime().getTime());
		return ourJavaTimestampObject;

	}

	public static Integer getUserAge(Date dateOfBirth) {
		
		GregorianCalendar today = new GregorianCalendar();
	    GregorianCalendar bday = new GregorianCalendar();
	    GregorianCalendar bdayThisYear = new GregorianCalendar();

	    bday.setTime(dateOfBirth);
	    bdayThisYear.setTime(dateOfBirth);
	    bdayThisYear.set(Calendar.YEAR, today.get(Calendar.YEAR));

	    int age = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);

	    if(today.getTimeInMillis() < bdayThisYear.getTimeInMillis())
	        age--;

	    System.out.println(age);
	    return age;
	}
	
	public static void main(String[] args) throws ApplicationException {
		getUserAge(getDateFromStr("06/12/1984"));
	}

}
