package com.xlrs.commons.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncryptionUtil {

	private static String secretKey = "s3cr3t-3ncr4ptlm";
	private static String salt = "3ncr4pt!0n-5A1T";
	private static String ENCRYPTION_TYPE = "AES";
	private static String SECRET_KEY_TYPE= "PBKDF2WithHmacSHA256";
	private static String CIPHER_TYPE = "AES/CBC/PKCS5PADDING";
	private static String ENCODING_TYPE = "UTF-8";

	public static String decrypt(String strToDecrypt) {
		try {
			return decryptInput(strToDecrypt);
		} catch (Exception e) {
			log.error("Faild to decript string. Reason is "+e.getMessage());
		}
		return null;
	}
	
	public static String encrypt(String strToEncrypt) {
		try {
			return encryptInput(strToEncrypt);
		} catch (Exception e) {
			log.error("Faild to encript string. Reason is "+e.getMessage());
		}
		return null;
	}
	
	private static String encryptInput(String strToEncrypt) throws Exception {

		byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		IvParameterSpec ivspec = new IvParameterSpec(iv);

		SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_TYPE);
		KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), ENCRYPTION_TYPE);

		Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
		return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(ENCODING_TYPE)));
	}

	private static String decryptInput(String strToDecrypt) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException  {
		byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		IvParameterSpec ivspec = new IvParameterSpec(iv);

		SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_TYPE);
		KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), ENCRYPTION_TYPE);

		Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
		cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
		return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
	}

	public static void main(String[] args) throws Exception {

		String originalString = "rsengar1284";
		String encryptedString = encrypt(originalString);
		String decryptedString = decrypt("68etn8UzH3pIjG05y0ErEdpmIAmi6AQuHK47rBJiNhsycHBrNeYQjRM6rL5uEhZabwFqcRJv5BTGDqiyrJo5eBHZVLP1JsS6qtFquvenjkxoVntGX+fw15I7MwL3A98NugxyYYsvEA3wWC4ngjyVDb6uxS5Q+mrf+97hJ6+CRZqrVJRraifoJ+DrxQy95E0ORgK7EZtxwmmz+qJVzEBWbNPEx+aabBA3W2D5ycJeMpb6nHCiHEGIoq9hfADz4Lpmj9D+gbuf0wWi8JZ/6do5yAvupY4SK4IWrRMKzK+PvocIgGkG87BGwcSx3RhgrydY3DUbSeVTBBDvq5DYRn/9R8Y/2wt7GRgCc0URuquBKyqs2JqahngvcohsaQEoEvJX+lRjd0JTpRajA9m51lvrhVROOD1cyT1J9AS5qk6QDWbh/RW1H3YzWlrUDYcTXWhRfJM1UVDEktl/MbdqMkcImTlvojW2YhX2Ry5TjmSTq1nnwQknOlKHk1zBz1wXJ9hCdKOr/XyAsL8furf1JDTDCrXyaHPnJmK97knPQj8aNRU=");

		System.out.println(originalString);
		System.out.println(encryptedString);
		System.out.println(decryptedString);
	}
}
