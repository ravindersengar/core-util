package com.xlrs.commons.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommonUtil {

	public static String[] split(String s) {
		return split(s, ",");
	}

	public static String[] split(String string, String separator) {
		if (string != null)
			return string.split(separator);
		return null;
	}

	private static Random randomGen = new Random();

	public static String randomString(int length) {
		int[][] alphaChars = { { 65, 90 }, { 97, 122 } };
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int[] range = alphaChars[randomGen.nextInt(1)];
			sb.append((char) nextIntInRange(range[0], range[1]));
		}
		return sb.toString();
	}

	public static int nextIntInRange(int min, int max) {
		return min + randomGen.nextInt(max - min);
	}

	public static String generateOTPForMobile(int len) {
		log.debug("Your new mobilepassword is : ");

		String numbers = "0123456789";
		String values = numbers;
		Random rndm_method = new Random();

		char[] password = new char[len];
		String otp = "";

		for (int i = 0; i < len; i++) {
			password[i] = values.charAt(rndm_method.nextInt(values.length()));
			otp = otp+values.charAt(rndm_method.nextInt(values.length()));
		}
		return otp;
	}
	
	public static String generateOTPForEmail(int len) {
		//log.debug("Your new email password is : ");

		String values = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
		Random rndm_method = new Random();

		char[] password = new char[len];
		String otp = "";

		for (int i = 0; i < len; i++) {
			password[i] = values.charAt(rndm_method.nextInt(values.length()));
			otp = otp+values.charAt(rndm_method.nextInt(values.length()));
		}
		return otp;
	}
	
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		Integer length = 1000000;
		for (int i = 0; i < length; i++) {
			String generateOTP = generateOTPForEmail(10);
			set.add(generateOTP);
			System.out.println(generateOTP);
		}
		System.out.println(length - set.size());
		
	}

}
