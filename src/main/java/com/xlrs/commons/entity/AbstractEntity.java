package com.xlrs.commons.entity;

import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@MappedSuperclass
@Data
@EntityListeners(AuditingEntityListener.class)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AbstractEntity implements BaseEntity{

	private static final long serialVersionUID = -7712759387623541544L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Long id;
	
	private String status;
	
	@JsonIgnore
	private Long createdBy;
	
	@JsonIgnore
	private Long updatedBy;
	
	@JsonIgnore
    private Date createdAt;
	
	@JsonIgnore
    private Date updatedAt;
    
    @PrePersist
    protected void prePersist() {
        if (this.createdAt == null) createdAt = new Date();
        if (this.updatedAt == null) updatedAt = new Date();
        
        if (this.createdBy == null) createdBy = 0L;
        if (this.updatedBy == null) updatedBy = 0L;
    }
    

    @PreUpdate
    protected void preUpdate() {
    	if(this.updatedBy==null) {
    		this.updatedAt = new Date();
            this.updatedBy = 0L;
    	}
    }

    @PreRemove
    protected void preRemove() {
    	if(this.updatedBy==null) {
    		this.updatedAt = new Date();
            this.updatedBy = 0L;
    	}
    }
	
}
