package com.xlrs.commons.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class BaseRepository<T extends AbstractEntity> {
	
	public T addAuditFeilds(T t, Long updatedBy, String status) {
		if(t.getId()==null) {
			t.setCreatedAt(new Date());
			t.setCreatedBy(updatedBy);
			t.setUpdatedAt(new Date());
			t.setUpdatedBy(updatedBy);
		}else {
			t.setUpdatedAt(new Date());
			t.setUpdatedBy(updatedBy);
		}
		t.setStatus(status);
		return t;
	}
}
