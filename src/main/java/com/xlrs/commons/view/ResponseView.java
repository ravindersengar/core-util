package com.xlrs.commons.view;


import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.constant.CommonConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class ResponseView implements Serializable{

	private static final long serialVersionUID = 1L;
	private HTTPStatus httpStatus; 
	private String responseStatus;

	private Object data;
	private ErrorView errorMessage;
	private List<ErrorView> errorMessages;

	public ResponseView(Object data) {
		super();
		this.httpStatus = HTTPStatus.OK;
		this.responseStatus = CommonConstants.STATUS_SUCCESS;
		this.data = data;
		this.errorMessage = null;
		this.errorMessages=null;
	}

	public ResponseView(HTTPStatus httpStatus, Object data, ErrorView errorMessage) {
		super();
		this.httpStatus = httpStatus;
		this.data = data;
		this.errorMessage = errorMessage;
	}

}
