package com.xlrs.commons.view;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EmailView implements Serializable {

	private static final long serialVersionUID = 1;

	private Long id;
	private Long recipientUserId;
	private String recipientMailId;
	private String requestType; // USER_REG,FORGET_PASS,ITR_SUBMIT . . .
	private String subject;
	private String contentType;
	private String body;
	private String templateName;
	private Map<String, Object> contentVariables;

}
