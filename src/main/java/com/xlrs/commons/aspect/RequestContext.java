package com.xlrs.commons.aspect;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestContext implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String requestTransactionId;
	
	private String securityToken;
	
	private String apiKey;
}
