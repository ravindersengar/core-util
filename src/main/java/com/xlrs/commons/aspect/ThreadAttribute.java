package com.xlrs.commons.aspect;

public class ThreadAttribute {
	private static InheritableThreadLocal<RequestContext> threadAttrs = new InheritableThreadLocal<RequestContext>() {
		@Override
		protected RequestContext initialValue() {
			return new RequestContext();
		}
	};

	public static void setReqTransactionId(String reqTranxId) {
		threadAttrs.get().setRequestTransactionId(reqTranxId);
	}

	public static String getReqTransactionId() {
		return threadAttrs.get().getRequestTransactionId();
	}
	
	public static void setSecurityToken(String securityToken) {
		threadAttrs.get().setSecurityToken(securityToken);
	}

	public static String getSecurityToken() {
		return threadAttrs.get().getSecurityToken();
	}
	
	public static void setApiKey(String apiKey) {
		threadAttrs.get().setApiKey(apiKey);
	}

	public static String getApiKey() {
		return threadAttrs.get().getApiKey();
	}
}
